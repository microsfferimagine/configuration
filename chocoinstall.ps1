#Requires -RunAsAdministrator

Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

Set-ExecutionPolicy Unrestricted

choco install -y mls-software-openssh
choco install -y openssl.light
choco install -y git.install
choco install -y git-lfs
choco install -y git-lfs-bitbucket-adapter
choco install -y poshgit 
choco install -y nuget.commandline
choco install -y microsoft-build-tools --version 14.0.25420.1 --force
choco install -y ruby

choco install -y googlechrome
choco install -y visualstudiocode
choco install -y teamviewer
choco install -y postman
choco install -y slack
refreshenv

choco install -y sql-server-express
choco install -y sql-server-management-studio
refreshenv

choco install -y visualstudio2017professional
choco install -y meld
choco install -y kdiff3
choco install -y dotnetcore-sdk
refreshenv

choco install -y typescript
choco install -y nodejs.install
choco install -y vagrant
choco install -y notepadplusplus.install

exit(1)
