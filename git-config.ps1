Write-Host
Write-Host "Configurando o Git..."
Write-Host 

$User = Read-Host -Prompt 'Qual o seu nome?'
$Email = Read-Host -Prompt 'Qual o seu e-mail?'

git config --global user.name "$User"
git config --global user.email "$Email"

git config --global core.autocrlf input 
git config --global core.safecrlf false 

git config --global color.ui true 

git config --global color.status.untracked "bold yellow" 
git config --global color.status.changed "bold yellow" 

git config --global color.branch.changed "bold yellow" 

Write-Host
Write-Host "Configurando ferramenta de merge e diff"
Write-Host 

git config --global --add merge.tool kdiff3
git config --global --add mergetool.kdiff3.path "C:/Program Files/KDiff3/kdiff3.exe"
git config --global --add mergetool.kdiff3.trustExitCode false

git config --global --add diff.guitool kdiff3
git config --global --add difftool.kdiff3.path "C:/Program Files/KDiff3/kdiff3.exe"
git config --global --add difftool.kdiff3.trustExitCode false

Get-Content ~/.gitconfig

Write-Host
Write-Host "Gerando a sua chave SSH"
Write-Host 

$ssh_dir = "$env:USERPROFILE\.ssh"

Remove-Item -Path  "$ssh_dir" -Recurse > $null
New-Item -ItemType Directory -Force -Path "$ssh_dir" > $null 
ssh-keygen -f "$ssh_dir\id_rsa" -t rsa -N "''" 

Write-Host
Write-Host "Copie a sua chave e informe no seu perfil do Bitbucket!"
Write-Host 

Get-Content $ssh_dir/id_rsa.pub
Write-Host 
