$proj =  "c:\proj"
$imagine = "$proj\imagine"
$sigposto = "$proj\SigPosto-6.0.7.9xxx"
$portal = "$proj\portal"

Remove-Item -Path  "$proj" -Force -Recurse > $null
New-Item -ItemType Directory -Force -Path $proj > $null 
New-Item -ItemType Directory -Force -Path $imagine > $null 
New-Item -ItemType Directory -Force -Path $sigposto > $null 
New-Item -ItemType Directory -Force -Path $portal > $null 

Set-Location $imagine

Write-Host
Write-Host "Clonando o Imagine Framework"
Write-Host
git clone git@bitbucket.org:microsfferimagine/framework.git
Write-Host
Write-Host "Clonando o Imagine Business"
Write-Host
git clone git@bitbucket.org:microsfferimagine/business.git
Write-Host
Write-Host "Clonando o Imagine Integrations"
Write-Host
git clone git@bitbucket.org:microsfferimagine/integrations.git
Write-Host
Write-Host "Clonando o Imagine Third Library"
Write-Host
git clone git@bitbucket.org:microsfferimagine/thirdpartlibrary.git
Write-Host
Write-Host "Clonando o Imagine Apps"
Write-Host
git clone git@bitbucket.org:microsfferimagine/apps.git
Write-Host
Write-Host "Clonando o Imagine Web"
Write-Host
git clone git@bitbucket.org:microsfferimagine/web.git

Set-Location $sigposto

Write-Host
Write-Host "Clonando o SigPosto src"
Write-Host
git clone git@bitbucket.org:microsffer/sigposto.git "src" 
Write-Host
Write-Host "Clonando o SigPosto Plugins"
Write-Host
git clone git@bitbucket.org:microsffer/plugins.git
Write-Host
Write-Host "Clonando o SigPosto lib"
Write-Host
git clone git@bitbucket.org:microsffer/sigposto-lib.git "lib" 
Write-Host
Write-Host "Clonando o SigPosto build"
Write-Host
git clone git@bitbucket.org:microsffer/sigposto-build.git "build"

Set-Location $portal

Write-Host
Write-Host "Clonando o Portal do Analista"
Write-Host
git clone git@bitbucket.org:orpak/fiscal-app.git 
Write-Host
Write-Host "Clonando o Portal do Guarda"
Write-Host
git clone git@bitbucket.org:microsfferimagine/guarda.web.git
Write-Host
Write-Host "Clonando o Portal da Raizen"
Write-Host
git clone git@bitbucket.org:orpak/raizen-monitor-app.git
