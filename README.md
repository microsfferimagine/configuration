# Configuração do Ambiente de Desenvolvimento #

Instalação de máquinas novas, configuração do Git e clonagem dos repositórios da Microsffer.

## Passos ##

---

* Abrir o **powershell** como **Administrator**.

* Antes de rodar o Scrip, rodar o comando: Set-ExecutionPolicy RemoteSigned

* Rodar o script _**chocoinstall.ps1**_ para instalar o Chocolatey e os packages para o ambiente de desenvolvimento.

* Atualizar com Windows Update.

* Reiniciar a máquina.

* Se o framework 3.5 não estiver instalado, executar os sequintes comandos:
  * Abrir o **powershell** como **Administrator**
  * Executar o seguinte comando: _choco install dotnet3.5_
  * Fechar o powershell.

* Abrir novamente o **powershell** com o seu usuário.  **Não poder ser administrador**!

* Executar o script de configuração do git: _**git-config.ps1**_

* Copiar a chave **SSH** e adicioná-la ao seu usuário do **bitbucket**.

* Executar o script para clonar os diretórios: _**microsffer-clone.ps1**_

* Ir para o diretório _C:\proj\SigPosto-6.0.7.9xxx\lib\Install\spwin4_frame20_

* Instalar o aplicativo do Spread: **SpWinv4.exe**

* Ir para o diretório _C:\proj\SigPosto-6.0.7.9xxx\lib\Install\CrystalReports_

* Instalar o Crystal Reports 64bits ou 32bits, dependendo do seu ambiente.

* Executar o build do **SIGposto**.

## Melhorias ##

---

* Software adicionais devem ser instalados com os pacotes do [**Chocolatey**.](https://chocolatey.org/packages)

* Qualquer eventual link que seja posteriormente quebrado deve ser atualizado. Este repositório é para todos manterem.
